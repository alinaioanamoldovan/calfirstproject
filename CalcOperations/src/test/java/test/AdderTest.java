package test;

import com.project.calcOOP.operation.implementation.Adder;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static org.hamcrest.CoreMatchers.is;

@RunWith(JUnit4.class)
public class AdderTest {

    private Adder adder;

    @Before
    public void setAdder() {
        adder = new Adder();
    }

    @Test
    public void testAdder() {
        Assert.assertThat(adder.operation(12.0, 19.0), is(31.0));
        Assert.assertThat(adder.operation(0, -19.0), is(-19.0));
        Assert.assertThat(adder.operation(190.0, 0), is(190.0));
        Assert.assertThat(adder.operation(-10.0, 20.0), is(10.0));
    }

}
