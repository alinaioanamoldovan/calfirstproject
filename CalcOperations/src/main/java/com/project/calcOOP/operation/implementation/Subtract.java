package com.project.calcOOP.operation.implementation;

import com.project.calcOOP.operation.Operation;

/**
 * This class represents the implementation of the binary operation subtract
 */
public class Subtract implements Operation {

    /**
     * This represents the mathematical operation symbol
     */
    private String operationSymbol;
    /**
     * This represents the operation category
     */
    private String operationCategory;

    public Subtract() {
        this.operationCategory = "binary";
        this.operationSymbol = "-";
    }

    /**
     * This method performs the mathematical operation
     *
     * @param arguments
     * @return the result of the operation
     */
    @Override
    public double operation(double... arguments) {
        double result = Double.MAX_VALUE;
        if (arguments.length == 2) {
            result = arguments[0] - arguments[1];
        }
        return result;
    }

    /**
     * @return the operation symbol
     */
    @Override
    public String getOperationSymbol() {
        return this.operationSymbol;
    }

    /**
     * @return operation category
     */
    @Override
    public String getOperationCategory() {
        return this.operationCategory;
    }
}
