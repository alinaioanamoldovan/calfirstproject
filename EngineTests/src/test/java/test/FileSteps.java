package test;

import com.project.calcOOP.main.MainClass;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class FileSteps {

    List<String> file1;
    List<String> file2;
    String outputFilePath;

    @When("^I have two files$")
    public void iHaveTwoFiles() throws IOException {

        ClassLoader classLoader = getClass().getClassLoader();
        String pathInputFile = classLoader.getResource("input.txt").getPath();
        String pathOutputFile = classLoader.getResource("output.txt").getPath();
        String pathExpectedOutput = classLoader.getResource("output_expected.txt").getPath();
        pathInputFile = pathInputFile.substring(1);
        outputFilePath = pathOutputFile.substring(1);
        pathExpectedOutput = pathExpectedOutput.substring(1);
        MainClass.main(pathInputFile, "output.txt");
        System.out.println(outputFilePath);
        file1 = Files.readAllLines(Paths.get(outputFilePath));
        file2 = Files.readAllLines(Paths.get(pathExpectedOutput));

    }

    @Then("^The content should be the same$")
    public void theContentShouldBe() {
        assertEquals("The size of the files should be the same", file1.size(), file2.size());

        for (int i = 0; i < file1.size(); i++) {
            System.out.println("Comparing line: " + i);
            assertEquals("The lines should be equal", file1.get(i), file2.get(i));
        }
    }
}
