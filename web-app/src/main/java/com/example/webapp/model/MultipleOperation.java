package com.example.webapp.model;

public class MultipleOperation {

    private String bodyOperation;

    public MultipleOperation() {
    }

    public  String getBodyOperation() {
        return this.bodyOperation;
    }

    public void setBodyOperation(final String bodyOperation) {
        this.bodyOperation = bodyOperation;
    }
}
