package test;

import com.project.calcOOP.operation.implementation.Subtract;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;

@RunWith(JUnit4.class)
public class SubstractTest {

    private Subtract substract;

    @Before
    public void setSubstract() {
        substract = new Subtract();
    }

    @Test
    public void testSubstract() {
        Assert.assertThat("The result should be the same", substract.operation(10.0, 5.0), is(5.0));
        Assert.assertThat("The result should be the same", substract.operation(10.0, -5.0), is(15.0));
        Assert.assertThat("The result should not be the same", substract.operation(10.0, 15.0), not(5.0));

    }
}
