package com.project.calcOOP.calculator.implementation;

import com.project.calcOOP.calculator.Calculator;
import com.project.calcOOP.operation.Operation;
import javafx.util.Pair;

import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

public class CalculatorImpl extends Calculator {
    Properties mainProperties;

    {
        loadProperties();
        loadOperationsMap();
    }

    @Override
    public double[] getOperands() {
        return this.operands;
    }

    @Override
    public String getSymbolOperation() {
        return this.symbol;
    }

    @Override
    public void setOperands(double[] operands) {
        this.operands = operands;
    }

    @Override
    public void setSymbolOperation(String symbolOperation) {
        this.symbol = symbolOperation;
    }

    @Override
    public Pair<Double, String> makeCalculations() {
        Operation operation = null;
        String symbol;
        double result = Double.MAX_VALUE;
        if (operationMap.containsKey(this.getSymbolOperation())) {
            operation = operationMap.get(this.getSymbolOperation());
            result = operation.operation(operands);
        }
        double finalResult = result;
        Operation finalOperation = operation;
        if (finalOperation == null) {
            symbol = "default";
        } else {
            symbol = finalOperation.getOperationSymbol();
        }
        return new Pair<>(finalResult, symbol);
    }

    @Override
    public Operation getOperation() {
        return operationMap.get(this.symbol);
    }

    public Operation getOperationInstance(String className) {
        Operation operation = null;
        try {
            if (className != null) {
                Class cls = Class.forName(className);
                operation = (Operation) cls.newInstance();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        return operation;
    }

    public void loadProperties() {
        Path currentRelativePath = Paths.get("");
        String s = currentRelativePath.toAbsolutePath().toString();
        System.out.println(s);
        mainProperties = new Properties();
        try {
            mainProperties.load(new FileInputStream(s + "\\application.properties"));
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private void loadOperationsMap() {
        Set<Map.Entry<Object, Object>> entries = mainProperties.entrySet();
        for (Map.Entry<Object, Object> entry : entries) {
            operationMap.put(String.valueOf(entry.getKey()), getOperationInstance(String.valueOf(entry.getValue())));
        }
        operationMap.put("+", getOperationInstance(mainProperties.getProperty("add")));
    }
}
