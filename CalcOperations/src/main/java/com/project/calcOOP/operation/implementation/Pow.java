package com.project.calcOOP.operation.implementation;

import com.project.calcOOP.operation.Operation;

public class Pow implements Operation {

    /**
     * This represents the mathematical symbol
     */
    private String operationSymbol;
    /**
     * This represents the operation operationCategory
     */
    private String operationCategory;

    public Pow() {
        this.operationSymbol = "^";
        this.operationCategory = "binary";
    }

    @Override
    public double operation(double... arguments) {
        double result = Double.MAX_VALUE;
        if (arguments.length == 2) {
            result = Math.pow(arguments[0], arguments[1]);
        }
        return result;
    }

    @Override
    public String getOperationSymbol() {
        return this.operationSymbol;
    }

    @Override
    public String getOperationCategory() {
        return this.operationCategory;
    }
}
