package com.example.webapp.model;

public class Operation {
    private String operation;
    private String operands;
    private double result;

    public Operation() {
    }

    public String getOperation() {
        return this.operation;
    }

    public void setOperation(final String operation) {
        this.operation = operation;
    }

    public String getOperands() {
        return this.operands;
    }

    public void setOperands(final String operands) {
        this.operands = operands;
    }

    public double getResult() {
        return this.result;
    }

    public void setResult(final double result) {
        this.result = result;
    }


    @Override
    public String toString() {
        return "operation='" + operation + '\'' + "," +
               "operands='" + operands + '\'' + "," +
               "result=" + result;
    }
}
