package com.project.calcOOP.operation.implementation;

import com.project.calcOOP.operation.Operation;

public class Inverse implements Operation {

    private String operationSymbol;
    private String operationCategory;

    public Inverse() {
        this.operationCategory = "unary";
        this.operationSymbol = "inv";
    }

    @Override
    public double operation(double... arguments) {
        double result = Double.MAX_VALUE;
        if (arguments.length > 0) {
            result = 1.0 / arguments[0];
        }
        return result;
    }

    @Override
    public String getOperationSymbol() {
        return this.operationSymbol;
    }

    @Override
    public String getOperationCategory() {
        return this.operationCategory;
    }
}
