package com.project.calcOOP.operation;

/**
 * Calculator API
 * In order to make usage of the functionality you need to implement this interface
 */
public interface Operation {

    /**
     * Implements the mathematic operation
     *
     * @param arguments
     * @return the result  of the operation
     */
    double operation(double... arguments);

    /**
     * This method returns the operation symbol(example +,-,/ etc)
     *
     * @return
     */
    String getOperationSymbol();

    /**
     * This method returnes the type of the operation(example binary,multiple,unary)
     *
     * @return
     */
    String getOperationCategory();
}
