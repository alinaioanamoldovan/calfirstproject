package com.project.calcOOP.operation.implementation;

import com.project.calcOOP.operation.Operation;

/**
 * This class represents the implementation of the binary mathematical operation divide
 */
public class Divide implements Operation {

    private String operationSymbol;
    private String operationCategory;

    public Divide() {
        this.operationCategory = "binary";
        this.operationSymbol = "/";
    }

    @Override
    public double operation(double... arguments) {
        double result = Double.MAX_VALUE;
        if (arguments.length == 2) {
            if (arguments[1] != 0) {
                result = arguments[0] / arguments[1];
            }
        }
        return result;
    }

    @Override
    public String getOperationSymbol() {
        return this.operationSymbol;
    }

    @Override
    public String getOperationCategory() {
        return this.operationCategory;
    }
}
