------------------------------
||                          ||
|| USAGE OF THE APPLICATION ||
||                          ||
------------------------------

Requirements
------------

    * at least jre 1.8

What you have
-------------
    * web-app-0.0.1-SNAPSHOT.jar
    * application.properties -this should be in the same folder as the web app jar 

Execution
---------

    You can easily run the application with opening a command promp and execution the following command:
       java -jar web-app-0.0.1-SNAPSHOT.jar


