package com.project.calcOOP.operation.implementation;

import com.project.calcOOP.operation.Operation;

/**
 * This class represents the implementation of the binary operation multiply
 */
public class Multiply implements Operation {

    /**
     * mathematical operation symbol
     */
    private String operationSymbol;
    /**
     * operation category
     */
    private String operationCategory;

    /**
     * Instantiates a new Multiply.
     */
    public Multiply() {
        this.operationCategory = "binary";
        this.operationSymbol = "*";
    }

    /**
     * @param arguments for the operation
     * @return the result of the operation
     */
    @Override
    public double operation(double... arguments) {
        double result = Double.MAX_VALUE;
        if (arguments.length == 2) result = arguments[0] * arguments[1];
        return result;
    }

    /**
     * @return operation symbol
     */
    @Override
    public String getOperationSymbol() {
        return this.operationSymbol;
    }

    /**
     * @returns the operation category
     */
    @Override
    public String getOperationCategory() {
        return this.operationCategory;
    }
}
