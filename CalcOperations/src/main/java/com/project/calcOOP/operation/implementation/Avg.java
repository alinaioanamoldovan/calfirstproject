package com.project.calcOOP.operation.implementation;

import com.project.calcOOP.operation.Operation;

import java.util.stream.DoubleStream;

public class Avg implements Operation {

    private String operationSymbol;
    private String operationCategory;

    public Avg() {
        this.operationCategory = "multiple";
        this.operationSymbol = "avg";
    }

    @Override
    public double operation(double... arguments) {
        double result = Double.MAX_VALUE;
        if (arguments.length > 0) {
            result = DoubleStream.of(arguments).sum() / arguments.length;
        }
        return result;
    }

    @Override
    public String getOperationSymbol() {
        return this.operationSymbol;
    }

    @Override
    public String getOperationCategory() {
        return this.operationCategory;
    }
}
