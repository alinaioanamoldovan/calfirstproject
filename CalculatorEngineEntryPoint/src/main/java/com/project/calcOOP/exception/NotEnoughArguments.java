package com.project.calcOOP.exception;

public class NotEnoughArguments extends Exception {

    public NotEnoughArguments(String message) {
        super(message);
    }
}
