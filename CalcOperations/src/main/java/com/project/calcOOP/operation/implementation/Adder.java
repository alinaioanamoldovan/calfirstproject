package com.project.calcOOP.operation.implementation;

import com.project.calcOOP.operation.Operation;

import java.util.stream.DoubleStream;

/**
 * This class represents the implementation of the binary operation add
 */
public class Adder implements Operation {

    /**
     * This represents the mathematical symbol
     */
    private String operationSymbol;
    /**
     * This represents the operation operationCategory
     */
    private String operationCategory;

    public Adder() {
        this.operationSymbol = "+";
        this.operationCategory = "binary";
    }

    /**
     * This method performs the associated mathematical operation
     *
     * @param arguments
     * @return the result
     */
    @Override
    public double operation(double... arguments) {
        double result = Double.MAX_VALUE;
        if (arguments.length == 2) result = DoubleStream.of(arguments).sum();
        return result;
    }

    /**
     * This method retrieves the mathematical operation symbol
     *
     * @return the symbol
     */
    @Override
    public String getOperationSymbol() {
        return this.operationSymbol;
    }

    /**
     * Returns the operationCategory for the operation
     *
     * @return the operationCategory
     */
    @Override
    public String getOperationCategory() {
        return this.operationCategory;
    }
}
