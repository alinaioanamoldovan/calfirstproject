package com.project.calcOOP.calculator.contentprocess;

import com.project.calcOOP.calculator.Calculator;
import com.project.calcOOP.calculator.fileops.UtilsClass;
import com.project.calcOOP.calculator.implementation.CalculatorImpl;
import com.project.calcOOP.calculator.outputBuild.BuildResult;
import javafx.util.Pair;

public class ContentProcessing {

    public static void processContent(String inputFile, String outputFile) {
        BuildResult buildResult = new BuildResult();
        StringBuilder stringBuilder = new StringBuilder();
        String content = UtilsClass.readFromFile(inputFile);
        Calculator calculator = new CalculatorImpl();
        String[] contentSplited = content.split("\\r?\\n");
        for (String contentSplit : contentSplited) {
            Pair<String, double[]> contentPair = processContentString(contentSplit);
            calculator.setSymbolOperation(contentPair.getKey());
            calculator.setOperands(contentPair.getValue());
            Pair<Double, String> result = calculator.makeCalculations();
            if (!(result.getKey() == Double.MAX_VALUE) && !(result.getValue().equals("default"))) {
                buildResult.buildResult(calculator.getOperation(), result.getValue(), contentPair.getValue(), result.getKey().doubleValue(), stringBuilder);
            }
        }

        UtilsClass.writeContent(stringBuilder.toString(), outputFile);
    }

    private static Pair<String, double[]> processContentString(String contentSplit) {
        String[] split = contentSplit.split("\\s+");
        String sign = split[0];
        double[] operands = new double[split.length - 1];
        for (int i = 1; i <= split.length - 1; i++) {
            try {
                operands[i - 1] = Double.valueOf(split[i]).doubleValue();
            } catch (NumberFormatException ex) {
                System.out.println(ex.getMessage());
            }

        }

        return new Pair<>(sign, operands);
    }
}
