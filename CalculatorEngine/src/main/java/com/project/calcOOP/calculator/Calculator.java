package com.project.calcOOP.calculator;

import com.project.calcOOP.operation.Operation;
import javafx.util.Pair;

import java.util.HashMap;
import java.util.Map;

public abstract class Calculator {

    public double[] operands;
    public String symbol;

    public Map<String, Operation> operationMap = new HashMap<>();

    public abstract double[] getOperands();

    public abstract String getSymbolOperation();

    public abstract void setOperands(double[] operators);

    public abstract void setSymbolOperation(String operand);

    public abstract Pair<Double, String> makeCalculations();

    public abstract Operation getOperation();

}