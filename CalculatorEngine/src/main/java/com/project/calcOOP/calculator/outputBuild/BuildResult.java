package com.project.calcOOP.calculator.outputBuild;

import com.project.calcOOP.operation.Operation;

public class BuildResult {

    public void buildResult(Operation operation, String symbol, double[] operands, double result, StringBuilder stringBuilder) {

        if (operation.getOperationCategory().equals("binary")) {
            BuildBinaryOperation.buildBinaryOperation(stringBuilder, symbol, operands[0], operands[1], result);
        } else {
            if (operation.getOperationCategory().equals("unary")) {
                BuildUnaryOperation.buildUnaryOperation(stringBuilder, symbol, operands, result);
            } else {
                BuildMultipleParamsOperation.buildMultipleOperation(stringBuilder, ",", symbol, operands, result);
            }
        }

    }
}
