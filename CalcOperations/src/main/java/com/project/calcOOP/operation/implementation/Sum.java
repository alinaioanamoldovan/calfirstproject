package com.project.calcOOP.operation.implementation;

import com.project.calcOOP.operation.Operation;

import java.util.stream.DoubleStream;

/****
 * This class represents the implementation of the operation sum with multiple operators
 */
public class Sum implements Operation {

    private String operationSymbol;
    private String operationCategory;

    public Sum() {
        this.operationCategory = "multiple";
        this.operationSymbol = "sum";
    }

    @Override
    public double operation(double... arguments) {
        double result = Double.MAX_VALUE;
        if (arguments.length > 0) {
            result = DoubleStream.of(arguments).sum();
        }
        return result;
    }

    @Override
    public String getOperationSymbol() {
        return this.operationSymbol;
    }

    @Override
    public String getOperationCategory() {
        return this.operationCategory;
    }
}
