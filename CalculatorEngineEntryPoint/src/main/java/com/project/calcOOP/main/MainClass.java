package com.project.calcOOP.main;

import com.project.calcOOP.calculator.contentprocess.ContentProcessing;
import com.project.calcOOP.exception.NotEnoughArguments;

public class MainClass {

    public static void main(String... args) {
        NotEnoughArguments notEnoughArguments = new NotEnoughArguments("The program requires 2 arguments. The name of the input file to be processed and the name of the output file");
        if (args.length < 2) {
            System.out.println(notEnoughArguments.getMessage());
        } else {
            ContentProcessing.processContent(args[0], args[1]);
        }
    }
}
