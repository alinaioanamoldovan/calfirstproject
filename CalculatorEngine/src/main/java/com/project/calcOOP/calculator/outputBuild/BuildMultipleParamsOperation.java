package com.project.calcOOP.calculator.outputBuild;

public class BuildMultipleParamsOperation {

    public static void buildMultipleOperation(StringBuilder stringBuilder, String separator, String symbol, double[] operand, double result) {
        int position = 0;
        while (position < operand.length) {
            if (position == 0) {
                stringBuilder.append(symbol + "(");
            }
            if (position < operand.length - 1) {
                stringBuilder.append(operand[position]);
                stringBuilder.append(separator);
                stringBuilder.append(" ");
            } else {
                stringBuilder.append(operand[position]+")");
            }
            position++;
        }
        appendResult(stringBuilder, result);
    }


    private static void appendResult(StringBuilder stringBuilder, double result) {
        stringBuilder.append(" = ");
        stringBuilder.append(String.format("%.1f", result));
        stringBuilder.append("\n");
    }
}
