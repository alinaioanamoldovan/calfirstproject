package com.example.webapp.model;

import java.util.Arrays;

public class OperationForMapper {

    private String operation;
    private double[] operands;


    public OperationForMapper() {
    }

    public String getOperation() {
        return this.operation;
    }

    public void setOperation(final String operation) {
        this.operation = operation;
    }

    public double[] getOperands() {
        return this.operands;
    }

    public void setOperands(final double... operandsList) {
        this.operands = new double[operandsList.length];
        for (int i = 0; i < operandsList.length; i++) {
            operands[i] = operandsList[i];
        }
    }

    @Override
    public String toString() {
        return Arrays.toString(operands).replace("[", "").replace("]", "").trim();
    }
}
