package com.example.webapp.controller;

import com.example.webapp.model.Operation;
import com.project.calcOOP.calculator.Calculator;
import com.project.calcOOP.calculator.implementation.CalculatorImpl;
import javafx.util.Pair;
import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.Arrays;

@Controller
public class OperationController {

    @RequestMapping(value = "/operation", method = RequestMethod.GET)
    public String operation(Model model) {
        Operation operation = new Operation();
        operation.setOperation("");
        operation.setOperands("");
        operation.setResult(0.0);
        model.addAttribute("operation", operation);
        return "executeOperation";
    }

    @RequestMapping(value = "/operation", method = RequestMethod.POST)
    public String result(@ModelAttribute Operation operation) {
        Calculator calculator = new CalculatorImpl();
        if (operation.getOperands().isEmpty()) {
            return "errorOperands";
        }
        refactorInputData(operation);
        String[] listOperands = operation.getOperands().split(",");
        double[] operands = new double[listOperands.length];
        if (getOperands(listOperands, operands)) {
            return "errorNumbers";
        }
        if (calculator.operationMap.get(operation.getOperation()) == null) {
            return "error";
        }
        final String x = checkOperation(operation, calculator, operands);
        if (x != null) {
            return x;
        }
        calculator.setSymbolOperation(operation.getOperation());
        calculator.setOperands(operands);
        Pair<Double, String> result = calculator.makeCalculations();
        operation.setOperands(Arrays.asList(operation.getOperands()).toString());
        operation.setResult(result.getKey());
        return "result";
    }

    public boolean getOperands(String[] listOperands, double[] operands) {
        for (int nr = 0; nr < listOperands.length; nr++) {
            if (NumberUtils.isNumber(listOperands[nr])) {
                operands[nr] = Double.valueOf(listOperands[nr]);
            } else {
                return true;
            }
        }
        return false;
    }

    public String checkOperation(@ModelAttribute Operation operation, Calculator calculator, double[] operands) {
        String category = calculator.operationMap.get(operation.getOperation()).getOperationCategory();
        switch (category) {
            case "binary":
                if (operands.length != 2) {
                    return "errorOperands";
                }
            case "multiple":
                if (operands.length < 1) {
                    return "errorOperands";
                }
                break;
            case "unary":
                if (operands.length < 1 || operands.length > 1) {
                    return "errorOperands";
                }
                break;

        }
        return null;
    }

    public void refactorInputData(@ModelAttribute Operation operation) {
        operation.getOperands().replace('[', ' ');
        operation.getOperands().replace(']', ' ');
        operation.getOperands().trim();
    }
}
