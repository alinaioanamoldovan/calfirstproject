package com.project.calcOOP.calculator.outputBuild;

public class BuildBinaryOperation {

    public static void buildBinaryOperation(StringBuilder stringBuilder, String symbol, double operand1, double operand2, double result) {
        stringBuilder.append(operand1);
        stringBuilder.append(" ");
        stringBuilder.append(symbol);
        stringBuilder.append(" ");
        stringBuilder.append(operand2);
        appendResult(stringBuilder, result);

    }

    private static void appendResult(StringBuilder stringBuilder, double result) {
        stringBuilder.append(" = ");
        stringBuilder.append(String.format("%.1f", result));
        stringBuilder.append("\n");
    }
}
