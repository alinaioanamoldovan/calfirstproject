------------------------------
||                          ||
|| USAGE OF THE APPLICATION ||
||                          ||
------------------------------

Requirements
------------

    * at least jre 1.8

What you have
-------------
    * EngineEntryPoint-1.0-SNAPSHOT.jar (containing the calc-operation-api-1.0-SNAPSHOT.jar and calc-operation-impl-1.0-SNAPSHOT.jar in it)
    * EngineAPI-1.0-SNAPSHOT.jar
    * EngineCalculator-1.0-SNAPSHOT.jar
    * EngineOperations-1.0-SNAPSHOT.jar
    * input.txt
    * application.properties

Execution
---------

    You can easily run the application with opening a command promp and execution the following command:
        java -cp EngineEntryPoint-1.0-SNAPSHOT.jar;EngineCalculator-1.0-SNAPSHOT.jar;EngineAPI-1.0-SNAPSHOT.jar;EngineOperations-1.0-SNAPSHOT.jar com.calculatorOOP.main.MainClass input.txt output.txt

    Note:
        In the log there is a line in the end which says there isn't an operator for max keyword.
        If you want to extend the application for solving this issue, please read the "EXTENDING THE APPLICATION" part

-------------------------------
||                           ||
|| EXTENDING THE APPLICATION ||
||                           ||
-------------------------------

Extension
---------
    For extending the application you have to add a new line in the application.properties file.
    Please add the following row in the end:
        avg=com.project.calcOOP.operation.implementation.Avg and the structure is the following
        operation-name=path to the implementation

