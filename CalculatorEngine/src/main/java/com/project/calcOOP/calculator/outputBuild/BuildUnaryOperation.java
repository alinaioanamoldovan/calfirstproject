package com.project.calcOOP.calculator.outputBuild;

public class BuildUnaryOperation {


    public static void buildUnaryOperation(StringBuilder stringBuilder, String symbol, double[] operand, double result) {
        stringBuilder.append(symbol + "(");
        stringBuilder.append(operand[0]);
        stringBuilder.append(")");
        appendResult(stringBuilder, result);
    }

    private static void appendResult(StringBuilder stringBuilder, double result) {
        stringBuilder.append(" = ");
        stringBuilder.append(String.format("%.1f", result));
        stringBuilder.append("\n");
    }
}
