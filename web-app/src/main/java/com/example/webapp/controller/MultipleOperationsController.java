package com.example.webapp.controller;

import com.example.webapp.mapper.OperationMapper;
import com.example.webapp.model.MultipleOperation;
import com.example.webapp.model.Operation;
import com.example.webapp.model.OperationForMapper;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.project.calcOOP.calculator.Calculator;
import com.project.calcOOP.calculator.implementation.CalculatorImpl;
import javafx.util.Pair;
import org.apache.commons.lang3.math.NumberUtils;
import org.json.JSONObject;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Controller
public class MultipleOperationsController {

    final List<Operation> results = new ArrayList<>();

    @RequestMapping(value = "/executeMultipleOperations", method = RequestMethod.GET)
    public String getOperations(Model model) {
        MultipleOperation operation = new MultipleOperation();
        operation.setBodyOperation("");
        model.addAttribute("multipleOperation", new MultipleOperation());
        model.addAttribute("results", results);
        return "executeMultipleOperations";
    }

    @RequestMapping(value = "/executeMultipleOperations", method = RequestMethod.POST)
    public String processMultipleOperations(@ModelAttribute MultipleOperation multipleOperation) throws IOException {
        final Calculator calculator = new CalculatorImpl();
        final ObjectMapper mapper = new ObjectMapper();

        final List<OperationForMapper> myObjects = mapper.readValue(multipleOperation.getBodyOperation(), new TypeReference<List<OperationForMapper>>() {});
        final List<Operation> operations = myObjects.stream().map(OperationMapper::toOperation)
                                                    .collect(Collectors.toList());

        for (final Operation operation : operations) {
            final String[] listOperands = operation.getOperands().split(",");
            double[] operands = new double[listOperands.length];
            getOperands(listOperands, operands);
            calculator.setOperands(operands);
            calculator.setSymbolOperation(operation.getOperation());
            final Pair<Double, String> result = calculator.makeCalculations();
            operation.setOperands(Arrays.asList(operation.getOperands()).toString());
            operation.setResult(result.getKey());
            if (calculator.operationMap.containsKey(operation.getOperation()) && (operation.getResult() != Double.MAX_VALUE)) {
                JSONObject json = new JSONObject();// Convert text to object
                json.put("operation",operation.getOperation());
                json.put("operands",operation.getOperands());
                json.put("result",result.getKey());
                System.out.println(json.toString(4)); // Print it with specified indentation
                results.add(operation);
            }
        }
        return "executeMultipleOperations";
    }

    public boolean getOperands(String[] listOperands, double[] operands) {
        for (int nr = 0; nr < listOperands.length; nr++) {
            String trimmedObject = listOperands[nr].trim();
            if (NumberUtils.isNumber(trimmedObject)) {
                operands[nr] = Double.valueOf(trimmedObject);
            } else {
                return true;
            }
        }
        return false;
    }
}
