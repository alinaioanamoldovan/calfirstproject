package com.example.webapp.mapper;

import com.example.webapp.model.Operation;
import com.example.webapp.model.OperationForMapper;

public class OperationMapper {

    public static Operation toOperation(final OperationForMapper operationForMapper) {
        Operation operation = new Operation();
        operation.setOperation(operationForMapper.getOperation());
        operation.setOperands(operationForMapper.toString());
        operation.setResult(0d);
        return operation;
    }
}
