package com.project.calcOOP.calculator.fileops;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

public class UtilsClass {

    public static String readFromFile(String filePath) {
        StringBuilder contentBuilder = new StringBuilder();
        try (Stream<String> stream = Files.lines(Paths.get(filePath), StandardCharsets.UTF_8)) {
            stream.forEach(s -> contentBuilder.append(s).append("\n"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return contentBuilder.toString();
    }

    public static void writeContent(String content, String fileName) {
        try (PrintWriter bw = new PrintWriter(new FileWriter("./" + fileName))) {
            bw.print(content);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}