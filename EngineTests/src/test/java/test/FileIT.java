package test;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

@RunWith(Cucumber.class)
@CucumberOptions(
        plugin = "pretty",
        glue = "test",
        features = "classpath:features/file_feat.feature",
        strict = true,
        snippets = SnippetType.CAMELCASE
)
public class FileIT {
}



